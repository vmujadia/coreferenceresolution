# -*- coding: utf-8 -*-
import os
import sys
import imp
import copy
import socket
import random
import pickle
import codecs
import cPickle
import argparse
import collections
import numpy as np
from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
from sklearn.externals import joblib
from sklearn import tree
sys.path.append('API/concreteAPI')
import pronounclassify as pc
import anaphora_resolution_Locative as arl
import anaphora_resolution_Reflexive as arrf
import anaphora_resolution_Relative as arrl
import anaphora_resolution_FP as arfp
import anaphora_resolution_SP as arsp
import anaphora_resolution_TP as artp
import anaphora_resolution_MLbased as armlb
import ssfAPI_minimal as ssf
import markSpeakerSsf as mks



class concrete_anaphora_resolution_main():

    def check_found(self,value,fdic):
	for i in fdic:
		for j in fdic[i]:
			if value==j:
				return True,i
	return False,888
    def __init__(self,filePath, folderPath,preffile,modelpath,outpath):
	self.outpath=outpath
        self.filePath=filePath
	self.folderPath=folderPath
        self.pronounsInResolution=[]
        self.pronounResolutionModuleType=None
        self.useShallowFeature=None
        self.useDepFeature=None
        self.usesemprop=None
        self.useNER=None
        self.__pronouns={}
        self.prefFilepath=preffile
	self.modelpath=modelpath
        self=self.parse_preference()
        

    def parse_preference(self):
        preference_file=codecs.open(self.prefFilepath, 'r', 'utf-8')
        pre_data=preference_file.readlines()
        line_count=0;int_line_count=0
        stm_count=-1;current_pronoun=None
        j1=0;line_cc=None
        for i in pre_data:
            if i.strip()!='':
                if i[0]!='#':
                    i_split=i.split()
                    if line_count==0:
                        for j in i_split:
                            if '@'== j.strip():
                                continue
                            if '#' in j.strip():
                                break
			    self.pronounsInResolution.append(j.strip())
                    elif line_count==1:
                        for j in i_split:
                            if '@'== j.strip():
                                continue
                            if '#' in j.strip():
                                break
                            self.pronounResolutionModuleType=j.strip()
                    elif line_count==2:
                        for j in i_split:
                            if '@'== j.strip():
                                continue
                            if '#' in j.strip():
                                break
                            self.useShallowFeature=j.strip()
                    elif line_count==3:
                        for j in i_split:
                            if '@'== j.strip():
                                continue
                            if '#' in j.strip():
                                break
                            self.useDepFeature=j.strip()
                    elif line_count==4:
                        for j in i_split:
                            if '@'== j.strip():
                                continue
                            if '#' in j.strip():
                                break
                            self.usesemprop=j.strip()
                    elif line_count==5:
                        for j in i_split:
                            if '@'== j.strip():
                                continue
                            if '#' in j.strip():
                                break
                            self.useNER=j.strip()
                    else:
                        j1=0
                        if int_line_count==0:
                            current_pronoun=i_split[1].strip()
                            self.__pronouns[current_pronoun]={}
                        elif int_line_count==1:
                            self.__pronouns[current_pronoun]['PRPRoot']=[]
                            for j in i_split:
                                self.__pronouns[current_pronoun]['PRPRoot'].append(j.strip())
                        elif int_line_count==2:
                            self.__pronouns[current_pronoun]['PRPbackrefstm']=[]
                            self.__pronouns[current_pronoun]['PRPbackrefstm'].append(i_split[1])
                            stm_count=int(i_split[1])
                        if stm_count!=-1:
                            if int_line_count!=2:
                                for j in i_split:
                                    if '@'== j.strip():
                                        continue
                                    if '#' in j.strip():
                                        break
                                    if j1==0:
                                        line_cc=j.strip()
                                        self.__pronouns[current_pronoun][int(line_cc)]=[]
                                    else:
                                        self.__pronouns[current_pronoun][int(line_cc)].append(j.strip())
                                    j1=j1+1
                            stm_count=stm_count-1
                            if stm_count==-1:
                                int_line_count=-1
                        int_line_count=int_line_count+1
                    line_count=line_count+1
        return self

    def resolution_run(self):
        fileName=self.filePath
        newFileList = []
        anaphoraDic={}
        if True:
            xFileName = fileName.split('/')[-1]
            if xFileName == 'err.txt' or xFileName.split('.')[-1] in ['comments','bak'] or xFileName[:4] == 'task' :
                gf=0
            else :
                newFileList.append(fileName)
        ana_count=0
        for fileName in newFileList :
            d = ssf.Document(fileName)
	    dpre = mks.markSpeakerSsf_main(d)
	    d = dpre.markSpeakerSsf_run()
	    dpre = pc.pronounclassifymain()
	    d = dpre.classifiypronoun(self.folderPath+'/model/propredictionclass/',d)
            for tree in d.nodeList :
                for chunkNode in tree.nodeList :
                    for node in chunkNode.nodeList :
                        if node.type.strip() == 'PRP':
			    #print node.getAttribute('reftype')
                            s=chunkNode
                            chunkNode.isPronoun=True
                            for i in self.__pronouns:
                                if node.morphroot in self.pronounsInResolution:
                                    if (node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='reflexive'):
                                        s1=arrf.reflexive_resolution(node)
                                        s=s1.RFResolve()
				    elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='firstperson':
                                        s1=arfp.firstPerson_resolution(node)
                                        s=s1.FPResolve()
				    elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='locative':
                                        s1=arl.locative_resolution(node)
                                        s=s1.LResolve()
				    elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='secondperson':
                                        s1=arsp.secondPerson_resolution(node)
                                        s=s1.SPResolve()
				    elif (node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='relative'):
                                        s1=arrl.relative_resolution(node,chunkNode)
                                        s=s1.RResolve()
				    elif (node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='thirdperson'):
                                        s1=artp.thirdPerson_resolution(node,chunkNode)
                                        s=s1.TPResolve()
                            if not s.isPronounresolved:
                                for i in self.__pronouns:
                                        if node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='firstperson':
                                            s1=armlb.ML_base_resolution(node,'FP',self.modelpath)
                                            s=s1.MLResolve()
                                        elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='locative':
                                            s1=armlb.ML_base_resolution(node,'L',self.modelpath)
                                            s=s1.MLResolve()
                                        elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='secondperson':
                                            s1=armlb.ML_base_resolution(node,'SP',self.modelpath)
                                            s=s1.MLResolve()
                                        elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='relative':
                                            s1=armlb.ML_base_resolution(node,'JO',self.modelpath)
                                            s=s1.MLResolve()
                                        elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='reflexive':
                                            s1=armlb.ML_base_resolution(node,'R',self.modelpath)
                                            s=s1.MLResolve()
                                        elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='thirdperson':
                                            s1=armlb.ML_base_resolution(node,'VH',self.modelpath)
                                            s=s1.MLResolve()
                                        elif node.morphroot in self.__pronouns[i]['PRPRoot'] and i=='other':
                                            s1=armlb.ML_base_resolution(node,'O',self.modelpath)
                                            s=s1.MLResolve()
                            if True:
                                    if s.isPronounresolved:
                                        ana_count=ana_count+1
                                        if s.PrePronounrefnode in anaphoraDic:
                                            temp=s.PrePronounrefnode
                                            flag=True
                                            while flag:
                                                if temp in anaphoraDic:
                                                    temp=anaphoraDic[temp]
                                                else:
                                                    flag=False
                                            ##self.update_anaphoraInSSF(s, temp, ana_count)
                                            anaphoraDic[temp]=s
                                        else:
                                            ##self.update_anaphoraInSSF(s, s.PrePronounrefnode, ana_count)
                                            anaphoraDic[s.PrePronounrefnode]=s
				    #3else:
				    ##	for ij in s.nodeList:
					##	if ij.type=='PRP':
					##		ij.addAttribute('predicted_ref',str('UNK'))
                            break
	    fdic={}
	    count=0	
	    for i in anaphoraDic:
		flag,value=self.check_found(i,fdic)
		if not flag:
			flag,value =self.check_found(anaphoraDic[i],fdic)
		if not flag:
			fdic[count]=[]
			fdic[count].append(i)
			fdic[count].append(anaphoraDic[i])
			count = count + 1
		else:
			if i not in fdic[value]:
				fdic[value].append(i)
			if anaphoraDic[i] not in fdic[value]:
				fdic[value].append(anaphoraDic[i])
	    fi_dic={}
            count=1
	    for i in fdic:
		in_c=1
		in_dic={}
		for j in fdic[i]:
			listt=[]
			for k in j.nodeList:
				if k.type!='PSP':
					listt.append(k)
			in_dic[in_c]=listt
			in_c=in_c+1
		fi_dic[count]=in_dic
		count=count+1
            '''
	    for i in fi_dic:
		print i, fi_dic[i]
	    


            for tree in d.nodeList :
                        if self.outpath==None:
				ghs=0
		        	print tree.printSSFValue(allFeat=False).encode('utf-8')
			else:
				f = codecs.open(self.outpath,'a','utf-8')
				f.write(tree.printSSFValue(allFeat=False))
				f.close()
	    '''
	    return d,fi_dic
    def update_anaphoraInSSF(self,ana,ana_ant,number):
        for ii in ana.upper.nodeList:
            if ii==ana:
		if ana.upper.name!=ana_ant.upper.name:
			for ij in ii.nodeList:
				if ij.type=='PRP':
					ij.addAttribute('predicted_ref',str('..%'+ana_ant.upper.name+'%'+ana_ant.name))
		else:
			for ij in ii.nodeList:
				if ij.type=='PRP':
					ij.addAttribute('predicted_ref',str(ana_ant.name))
                break

