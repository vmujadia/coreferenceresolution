# -*- coding: utf-8 -*-
import os
import sys
import imp
import copy
import socket
import random
import pickle
import codecs
import cPickle
import argparse
import collections
import numpy as np
import ssfAPI_minimal as ssf
from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
from sklearn.externals import joblib
from sklearn import tree

sys.path.append('API/nominalAPI')
import wordnet_check as wnc
import stringmatch as strm
import semantic_sim as semsim
import dictonary_check as dictche
import dependency_match as depm
import abbreviation as abb
import get_potiential_entities as gpe
import match_firstname as mfn
import loaddic as ldic


def nominal_reference_resolution_main(d,filename,path):
	gpe_obj=gpe.get_potiential_entities()
	strm_obj=strm.stringmatch()
	depm_obj=depm.dependency_match()
	abb_obj=abb.abbreviation()
	wnc_obj=wnc.wordnet_check()
	semsim_obj=semsim.semantic_sim()
	mfn_obj=mfn.match_firstname()

        coref_dic=gpe_obj.get_potiential_entities(d)
        coref_dic=strm_obj.dumy_string_match(coref_dic)
        #coref_dic=depm_obj.dependancy_relation_match(coref_dic)
	coref_dic=depm_obj.dependancy_dumy_3(coref_dic)
        coref_dic=strm_obj.string_match_NNP(coref_dic)
        coref_dic=strm_obj.string_match_other_WE(coref_dic)
        coref_dic=strm_obj.string_match_other_adj(coref_dic)
        coref_dic=abb_obj.abbreaviation_match(coref_dic)
        coref_dic=abb_obj.abbreaviation_match_dep_bracketed(coref_dic)
        #coref_dic=wnc_obj.string_match_wordnet(coref_dic)
	coref_dic=mfn_obj.match_for_first_name(coref_dic)
        #coref_dic=semsim_obj.word2vec_match(coref_dic)

        fcoref_dic={}
        count = 0
        for i in coref_dic:
            if len(coref_dic[i])>0:
                count = count +1
                fcoref_dic[count] = coref_dic[i]

        #coref_dic=dictonary_match(coref_dic)
        fcoref_dic123=copy.deepcopy(fcoref_dic)
        ##for i in fcoref_dic123:
        ##    print 'new ::', i,'\n\t--->',fcoref_dic123[i]
        #    for j in fcoref_dic123[i]:
        #        print '\t',' '.join(value1.lex+' = '+value1.upper.upper.name for value1 in fcoref_dic123[i][j] if value1.lex!=None).encode('utf-8'),
        #        print
        #    print '\n\n'



            #for tree in d.nodeList:
            #    print tree.printSSFValue(allFeat=False).encode('utf-8')
	return d,fcoref_dic123
