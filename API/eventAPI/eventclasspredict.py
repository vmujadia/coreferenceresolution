# -*- coding: utf-8 -*-
def folderWalk(folderPath):
    import os
    fileList = []
    for dirPath , dirNames , fileNames in os.walk(folderPath) :
        for fileName in fileNames :
            fileList.append(os.path.join(dirPath , fileName))
    return fileList

def getnodefordep(tree,name):
	for i in tree.nodeList:
		if i.name==name:
			return i


def getnode(name,sen):
	for i in sen.nodeList:
		if i.name==name:
			return i

def findnearestverb(sen):
	for i in sen.nodeList:
		for j in i.nodeList:
			if 'VM'==j.type:
				return j
def getparentverb(node):
	if node.parent =='0':
		return node
	elif node.type in ['VGNF','VGF','NULL__VGF','VGNN','0']:
		return node
	else:
		return getparentverb(getnodefordep(node.upper,node.parent))

def findccp(sen,posn):
	for i in sen.nodeList:
		for j in i.nodeList:
			if posn!=None and j.getAttribute('posn')!=None:
				if i.name=='CCP' and int(posn)<int(j.getAttribute('posn')):
					return True
	return False

def alldistance(doc,node1,node2):
	sendist=0
	nodedist=0
	chunkdist=0
	count = 0
	for i in doc.nodeList:
		for k in i.nodeList:
			for j in k.nodeList:
				if node1.lex==j.lex and node1.upper.name==j.upper.name and node1.upper.upper.name==j.upper.upper.name:
					count = count +1
					if count==1:
						sendist=0
						chunkdist=0
						nodedist=0
					if count==2:
						return nodedist-1,chunkdist-1,sendist
				if node2.lex==j.lex and node2.upper.name==j.upper.name and node2.upper.upper.name==j.upper.upper.name:
					count = count +1
					if count==1:
						sendist=0
						chunkdist=0
						nodedist=0
					if count==2:
						return nodedist-1,chunkdist-1,sendist
				nodedist = nodedist + 1
			chunkdist = chunkdist + 1
		sendist = sendist +1

if __name__ == '__main__' :	
    import sys
    import imp
    import ssfAPI_minimal as ssf
    import random
    import numpy as np
    import cPickle
    from sklearn.feature_extraction import DictVectorizer
    from sklearn import tree
    from sklearn.externals import joblib
    from sklearn import tree
    import argparse
    import pickle
    reload(sys)
    sys.setdefaultencoding("utf-8")

    if True:
	inputPath = sys.argv[1]
    	fileList = folderWalk(inputPath)
	newFileList=fileList
    	#newFileList = []
	#newFileList.append(inputPath)
	for fileName in newFileList :
		#print '\n\n\n\nFile Name ::: ',fileName,'\n\n\n'
		d = ssf.Document(fileName)
		for tree in d.nodeList :
		    for chunkNode in tree.nodeList :
		        for node in chunkNode.nodeList:
				if node.type=='PRP' and node.getAttribute('reftype')=='E':
					#print node.morphroot.strip()
					ref = node.getAttribute('ref') 
					if ',' in ref:
						ref=ref.split(',')
					if '%' in ref:
						rsen=ref.split('%')[1]
						rchunk=ref.split('%')[2]
					else:
						rsen=tree.name
						rchunk=ref
					for tree1 in d.nodeList:
						if int(tree1.name)==int(tree.name)-1 or tree.name == tree1.name:
							for chunkNode1 in tree1.nodeList:
								if chunkNode1.type in ['VGNF','VGF','NULL__VGF','VGNN']:
									for node1 in chunkNode1.nodeList:
										if 'VM'==node1.type:
											if chunkNode1.getAttribute('voicetype')==None:
												chunkNode1.addAttribute('voicetype','')
											if chunkNode1.getAttribute('stype')==None:
												chunkNode1.addAttribute('stype','')
											if node.getAttribute('posn')==None:
												node.addAttribute('posn','')
																	
											if node.number==None:
												node.number=''
											if node.person==None:
												node.person=''
											if node.gender==None:
												node.gender=''
											if node.morphroot==None:
												node.morphroot=''
											if node1.number==None:
												node1.number=''
											if node1.person==None:
												node1.person=''
											if node1.gender==None:
												node1.gender=''
											if node1.morphroot==None:
												node1.morphroot=''
											ndi,cdi,sdi=alldistance(d,node,node1)
											if rchunk==chunkNode1.name and tree1.name==rsen:
												print 'nodedist='+str(ndi),'chunkdist='+str(cdi),'stmdist'+str(sdi),'number1='+node.number,'person1='+node.person,'gender1='+node.gender,'root1='+node.morphroot,'posn1='+node.getAttribute('posn'),'ccp='+str(findccp(tree,node.getAttribute('posn'))),'distse='+str(int(tree1.name)-int(tree.name)),'number2='+node1.number,'person2='+node1.person,'gender2='+node1.gender,'root2='+node1.morphroot,'stype='+chunkNode1.getAttribute('stype'),'voice='+chunkNode1.getAttribute('voicetype'),'Class=1'
											else:
													
												print 'nodedist='+str(ndi),'chunkdist='+str(cdi),'stmdist'+str(sdi),'number1='+node.number,'person1='+node.person,'gender1='+node.gender,'root1='+node.morphroot,'posn1='+node.getAttribute('posn'),'ccp='+str(findccp(tree,node.getAttribute('posn'))),'distse='+str(int(tree1.name)-int(tree.name)),'number2='+node1.number,'person2='+node1.person,'gender2='+node1.gender,'root2='+node1.morphroot,'stype='+chunkNode1.getAttribute('stype'),'voice='+chunkNode1.getAttribute('voicetype'),'Class=0'
					
		#break
