# -*- coding: utf-8 -*-
import sys
import imp
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle
from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
from sklearn.externals import joblib
from sklearn import tree
import argparse
import pickle
reload(sys)
sys.setdefaultencoding("utf-8")


def folderWalk(folderPath):
    import os
    fileList = []
    for dirPath , dirNames , fileNames in os.walk(folderPath) :
        for fileName in fileNames :
            fileList.append(os.path.join(dirPath , fileName))
    return fileList

def getnodefordep(tree,name):
	for i in tree.nodeList:
		if i.name==name:
			return i


def getnode(name,sen):
	for i in sen.nodeList:
		if i.name==name:
			return i

def findnearestverb(sen):
	for i in sen.nodeList:
		for j in i.nodeList:
			if 'VM'==j.type:
				return j
def getparentverb(node):
	if node.parent =='0':
		return node
	elif node.type in ['VGNF','VGF','NULL__VGF','VGNN','0']:
		return node
	else:
		return getparentverb(getnodefordep(node.upper,node.parent))

def findccp(sen,posn):
	for i in sen.nodeList:
		for j in i.nodeList:
			if posn!=None and j.getAttribute('posn')!=None:
				if i.name=='CCP' and int(posn)<int(j.getAttribute('posn')):
					return True
	return False

def alldistance(doc,node1,node2):
	sendist=0
	nodedist=0
	chunkdist=0
	count = 0
	for i in doc.nodeList:
		for k in i.nodeList:
			for j in k.nodeList:
				if node1.lex==j.lex and node1.upper.name==j.upper.name and node1.upper.upper.name==j.upper.upper.name:
					count = count +1
					if count==1:
						sendist=0
						chunkdist=0
						nodedist=0
					if count==2:
						return nodedist-1,chunkdist-1,sendist
				if node2.lex==j.lex and node2.upper.name==j.upper.name and node2.upper.upper.name==j.upper.upper.name:
					count = count +1
					if count==1:
						sendist=0
						chunkdist=0
						nodedist=0
					if count==2:
						return nodedist-1,chunkdist-1,sendist
				nodedist = nodedist + 1
			chunkdist = chunkdist + 1
		sendist = sendist +1




def resolve(d,node_pronoun,fileName,path):
	all_list=[]
	for tree in d.nodeList :
	    for chunkNode in tree.nodeList :
	        for node in chunkNode.nodeList:
			if node.type=='PRP' and node==node_pronoun:
				for tree1 in d.nodeList:
					if int(tree1.name)==int(tree.name)-1 or tree.name == tree1.name:
						for chunkNode1 in tree1.nodeList:
							if chunkNode1.type in ['VGNF','VGF','NULL__VGF','VGNN']:
								for node1 in chunkNode1.nodeList:
									if 'VM'==node1.type:
										if chunkNode1.getAttribute('voicetype')==None:
											chunkNode1.addAttribute('voicetype','')
										if chunkNode1.getAttribute('stype')==None:
											chunkNode1.addAttribute('stype','')
										if node.getAttribute('posn')==None:
											node.addAttribute('posn','')
										if node.number==None:
											node.number=''
										if node.person==None:
											node.person=''
										if node.gender==None:
											node.gender=''
										if node.morphroot==None:
											node.morphroot=''
										if node1.number==None:
											node1.number=''
										if node1.person==None:
											node1.person=''
										if node1.gender==None:
											node1.gender=''
										if node1.morphroot==None:
											node1.morphroot=''
										ndi,cdi,sdi=alldistance(d,node,node1)
										predict={}
										Features= 'nodedist='+str(ndi)+' '+'chunkdist='+str(cdi)+' '+'stmdist'+str(sdi)+' '+'number1='+node.number+' '+'person1='+node.person+' '+'gender1='+node.gender+' '+'root1='+node.morphroot+' '+'posn1='+node.getAttribute('posn')+' '+'ccp='+str(findccp(tree,node.getAttribute('posn')))+' '+'distse='+str(int(tree1.name)-int(tree.name))+' '+'number2='+node1.number+' '+'person2='+node1.person+' '+'gender2='+node1.gender+' '+'root2='+node1.morphroot+' '+'stype='+chunkNode1.getAttribute('stype')+' '+'voice='+chunkNode1.getAttribute('voicetype')+' '+'Class=1'
										path=path+'model/eventmodel/'
										measurment1 = [];measurment2 = []
										tmp1 = {};tmp2 = {};O=''
										i = Features.strip()
										itemp = i.split()
										for j in range(len(itemp)-1):
											k =itemp[j].split('=')
											if len(k)==2:
												tmp1[k[0]]=k[1]
											else:
												tmp1[k[0]]=''
										ptype= itemp[len(itemp)-1].split('=')
										measurment1.append(tmp1)
										vec1 = DictVectorizer(sparse=False)
										with open(path+'features.pkl','rb') as handle:
												vec1.vocabulary_=pickle.loads(handle.read())
										X = vec1.transform(measurment1)
										yescount=0
										nocount=0
										clf = joblib.load(path+'svm_svr')
										for ic in X:
											if clf.predict(ic)>0.1:
												yescount+=1
											else:
												nocount+=1

										clf = joblib.load(path+'t_dtr')
										for ic in X:
											if clf.predict(ic)>0.1:
												yescount+=1
											else:
												nocount+=1

										clf = joblib.load(path+'t_dtc')
										for ic in X:
											if clf.predict(ic)>0.1:
												yescount+=1
											else:
												nocount+=1

										clf = joblib.load(path+'svm_linearsvc')
										for ic in X:
											if clf.predict(ic)>0.1:
												yescount+=1
											else:
												nocount+=1
										clf = joblib.load(path+'linear_model.BayesianRidge')
										for ic in X:
											if clf.predict(ic) > 0.20:
												yescount+=1
											else:
												nocount+=1
										clf = joblib.load(path+'linear_model.Ridge (alpha = .5)')
										for ic in X:
											if clf.predict(ic) > 0.20:
												yescount+=1
											else:
												nocount+=1

										clf = joblib.load(path+'linear_model.RidgeCV(alphas=[0.1, 1.0, 10.0])')
										for ic in X:
											if clf.predict(ic)>0.20:
												yescount+=1
											else:
												nocount+=1
										predict['ve']=yescount
										predict['ng']=nocount
										predict['node']=node1
								all_list.append(predict)
	max_val=''
	max_num=0
	for i in all_list:
		if int(i['ve'])>max_num:
			max_num=i['ve']
			max_val=i['node']
	return max_val
