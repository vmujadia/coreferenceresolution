# -*- coding: utf-8 -*-
import copy
import collections
import codecs
import re
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle
import socket

class dependency_match():
    
    	def __init__(self):
		fdf='init'


	def get_matched_index_for_DepMatch (self,coref_dic,value,chainid):
	    for i in coref_dic:
		try :
		    if chainid !=i:
		        for j in coref_dic[i]:
		            if coref_dic[i][j][len(coref_dic[i][j])-1].upper.upper.name == value[len(value)-1].upper.upper.name:
		                drel1=coref_dic[i][j][len(coref_dic[i][j])-1].getAttribute('drel').split(':')
		                drel1R=drel1[0];drel1P=drel1[1]
		                drel2=value[len(value)-1].getAttribute('drel').split(':')
		                drel2R=drel2[0];drel2P=drel2[1]
		                if drel2R=='ccof' and value[len(value)-1].parent.getAttribute('drel').split(':')[0]=='nmod' and value[len(value)-1].parent.getAttribute('drel').split(':')[1]==coref_dic[i][j][len(coref_dic[i][j])-1].getAttribute('name'):
		                    #clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		                    #clientsocket.connect(('10.3.1.91', 8036))
		                    #clientsocket.send(coref_dic[i][j][len(coref_dic[i][j])-1].name+':'+value[len(value)-1].name) # server input
		                    #data = clientsocket.recv(99999)
		                    #data_arr = cPickle.loads(data)
		                    print ' '.join(v.lex for v in value),'====',' '.join(v.lex for v in coref_dic[i][j])
		                    return True , i
		                if (drel1R=='nmod' and value[len(value)-1].name==drel1P or (drel2R=='nmod' and coref_dic[i][j][len(coref_dic[i][j])-1].name==drel2P)) :
		                    totalscore=0.0
		                    count_c=0
		                    print ' '.join(v.lex for v in value),'====',' '.join(v.lex for v in coref_dic[i][j]),
		                    for g1 in coref_dic[i][j]:
		                        for g2 in value:
		                            count_c=count_c+1
		                            clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		                            clientsocket.connect(('10.3.1.91', 8036))
		                            clientsocket.send(g1.name+':'+g2.name) # server input
		                            data = clientsocket.recv(99999)
		                            data_arr = cPickle.loads(data)
		                            totalscore=totalscore+float(data_arr)
		                    print (totalscore/count_c),'end',value[len(value)-1].getAttribute('semprop'),coref_dic[i][j][len(coref_dic[i][j])-1].getAttribute('semprop')

		                    if (totalscore/count_c)>0.0015 and ((value[len(value)-1].getAttribute('semprop')==coref_dic[i][j][len(coref_dic[i][j])-1].getAttribute('semprop')) or (value[len(value)-1].getAttribute('semprop')=='rest' and value[len(value)-1].type in ['NNP','NNPC','NN']) or (coref_dic[i][j][len(coref_dic[i][j])-1].getAttribute('semprop')=='rest' and coref_dic[i][j][len(coref_dic[i][j])-1].type in ['NNP','NNPC'])):
		                        return True , i
		except:
		    pass
	    return False , 0


	def dependancy_relation_match(self,coref_dic):
	    fcoref_dic123=copy.deepcopy(coref_dic)
	    count = 1
	    for i in fcoref_dic123:
		for j in fcoref_dic123[i]:
		    flag , index = self.get_matched_index_for_DepMatch(coref_dic,fcoref_dic123[i][j],i)
		    if not flag:
		        continue
		    else:
		        for k in fcoref_dic123[i]:
		            coref_dic[index][len(coref_dic[index])+1]=fcoref_dic123[i][k]
		            if k in coref_dic[i]:
		                coref_dic[i].pop(k)
	    return coref_dic


	def get_matched_index_dumy_3 (self,coref_dic,value,chainid):
	    for i in coref_dic:
		try:
		    if chainid !=i:
		        for j in coref_dic[i]:
		            if coref_dic[i][j][len(coref_dic[i][j])-1].upper.upper.name == value[len(value)-1].upper.upper.name and coref_dic[i][j][len(coref_dic[i][j])-1].parent.name == value[len(value)-1].parent.name and ((coref_dic[i][j][len(coref_dic[i][j])-1].getAttribute('drel').split(':')[0]== 'k1' and value[len(value)-1].getAttribute('drel').split(':')[0]== 'k1s') or (coref_dic[i][j][len(coref_dic[i][j])-1].getAttribute('drel').split(':')[0]== 'k1s' and value[len(value)-1].getAttribute('drel').split(':')[0]== 'k1')):
		                #print coref_dic[i][j][len(coref_dic[i][j])-1].name,coref_dic[i][j][len(coref_dic[i][j])-1].parent.name,coref_dic[i][j][len(coref_dic[i][j])-1].upper.upper.name,' ---->',value[len(value)-1].name,value[len(value)-1].parent.name,value[len(value)-1].upper.upper.name
		                #if (coref_dic[i][j][len(coref_dic[i][j])-1].morphroot).strip() == (value[len(value)-1].morphroot).strip():
		                return True , i
		except:
		    pass
	    return False , 0

	def dependancy_dumy_3(self,coref_dic):
		fcoref_dic123=copy.deepcopy(coref_dic)
		count = 1
		for i in fcoref_dic123:
			for j in fcoref_dic123[i]:				
					flag , index = self.get_matched_index_dumy_3(coref_dic,fcoref_dic123[i][j],i)
					if not flag:
						continue
					else:
						for k in fcoref_dic123[i]:
							coref_dic[index][len(coref_dic[index])+1]=fcoref_dic123[i][k]
							if k in coref_dic[i]:
								coref_dic[i].pop(k)
		return coref_dic
