# -*- coding: utf-8 -*-
import copy
import collections
import codecs
import re
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle

class abbreviation():
    
    	def __init__(self):
		self.fdf='init'
		self.join_char=["ि", "ा", "ु", "ी", "ृ", "ू", "े", "ै", "ो", "्", "ौ"]

	
	def get_matched_index_for_abbreviation(self,coref_dic,value,chainid):
	    for i in coref_dic:
		if chainid != i:
		    for j in coref_dic[i]:
		        if  coref_dic[i][j][len(coref_dic[i][j])-1].morphroot!= None  and value[len(value)-1].morphroot != None:
		            i_count=0;str2=''
		            if len(coref_dic[i][j])>1:
		                for d in coref_dic[i][j]:
		                    if 'NN' in d.type:
		                        i_count=0
		                        for c in d.morphroot:
		                            if i_count==0:
		                                str2=str2+c
		                            elif i_count==1:
		                                if c in self.join_char:
		                                    str2=str2+c
		                                break
		                            i_count=i_count + 1
		                    else:
		                        str1=''
		                        break


		            i_count=0;str1=''
		            if len(value)>1:
		                for d in value:
		                    if 'NN' in d.type:
		                        i_count=0
		                        for c in d.morphroot:
		                            if i_count==0:
		                                str1=str1+c
		                            elif i_count==1:
		                                if c in self.join_char:
		                                    str1=str1+c
		                                break
		                            i_count=i_count + 1
		                    else:
		                        str1=''
		                        break
		            if str1.strip()==str(' '.join(f.morphroot for f in coref_dic[i][j])).strip() or str2.strip()==str(' '.join(f.morphroot for f in value)).strip():
		                return True,i
	    return False , 0


	def abbreaviation_match(self,coref_dic):
	    fcoref_dic123=copy.deepcopy(coref_dic)
	    count = 1
	    for i in fcoref_dic123:
		for j in fcoref_dic123[i]:
		    flag=False
		    if True:
		        flag , index = self.get_matched_index_for_abbreviation(coref_dic,fcoref_dic123[i][j],i)
		        if not flag:
		            continue
		        else:
		            for k in coref_dic[i]:
		                coref_dic[index][len(coref_dic[index])+1]=coref_dic[i][k]
		            coref_dic[i]=[]

	    return coref_dic



	def get_matched_index_for_abbreviation_dep_bracketed(self,coref_dic,value,chainid):
	    for i in coref_dic:
		if chainid != i:
		    for j in coref_dic[i]:
		        if  coref_dic[i][j][len(coref_dic[i][j])-1].morphroot!= None  and value[len(value)-1].morphroot != None:
		            dep1=coref_dic[i][j][len(coref_dic[i][j])-1].getAttribute('drel')
		            dep2=value[len(value)-1].getAttribute('drel')
		            if dep1!=None and dep2!=None and coref_dic[i][j][len(coref_dic[i][j])-1].upper.upper.name==value[len(value)-1].upper.upper.name:
		                if (dep2.split(':')[0]=='rs' and dep2.split(':')[1]==coref_dic[i][j][len(coref_dic[i][j])-1].lex) or (dep1.split(':')[0]=='rs' and dep1.split(':')[1]==value[len(value)-1].lex):
		                    return True,i
	    return False , 0

	def abbreaviation_match_dep_bracketed(self,coref_dic):
	    fcoref_dic123=copy.deepcopy(coref_dic)
	    count = 1
	    for i in fcoref_dic123:
		for j in fcoref_dic123[i]:
		    flag=False
		    if True:
		        flag , index = self.get_matched_index_for_abbreviation_dep_bracketed(coref_dic,fcoref_dic123[i][j],i)
		        if not flag:
		            continue
		        else:
		            for k in coref_dic[i]:
		                coref_dic[index][len(coref_dic[index])+1]=coref_dic[i][k]
		            coref_dic[i]=[]

	    return coref_dic
