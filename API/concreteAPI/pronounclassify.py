# -*- coding: utf-8 -*-



class pronounclassifymain():
	def __init__(self):
		'''
		concrete_list=[u'सभी ',u'उनका ',u'उन ',u'उनकी',u'उनके',u'उनको',u'उनमें',u'उनसे',u'उन्हें',u'उन्होंने',u'उस',u'उसका',u'उसकी',u'उसके',u'उसने',u'जिनकी',u'जिनके',u'जिन',u'जिनमें',u'जिनसे',u'जिन्हें',u'जिन्होंने',u'खुद']
		concrete_list_root=[u'हम',u'वो',u'वहाँ',u'मै',u'जहाँ',u'खुद',u'कौन',u'आपस',u'आप',u'अपना']
		event_list=[u'सब ',u'इसलिये',u'इसलिए']
		extra_list_root=[u'तभी',u'कोई',u'अभी',u'कुछ',u'अब',u'तो',u'किसी']
		time=[u'तब',u'जब']
		dic_total_pronoun_root={}
		dic_correct_pronoun_root={}
		dic_incorrect_pronoun_root={}
		dic_pronoun_dtc={}
		dic_pronoun_svclinear={}
		dic_pronoun_linearsvc={}
		dic_pronoun_bayesian={}
		dic_pronoun_ridge={}
		dic_total_pronoun={}
		dic_correct_pronoun={}
		dic_incorrect_pronoun={}
		'''
	def folderWalk(self,folderPath):
    		import os
    		fileList = []
    		for dirPath , dirNames , fileNames in os.walk(folderPath) :
       			for fileName in fileNames :
            			fileList.append(os.path.join(dirPath , fileName))
    		return fileList

	def classifiypronoun(self,path,upperd):	
    		import sys
    		import imp
    		import ssfAPI_minimal as ssf
		import random
    		import numpy as np
    		import cPickle
    		from sklearn.feature_extraction import DictVectorizer
    		from sklearn import tree
    		from sklearn.externals import joblib
    		from sklearn import tree
    		import argparse
    		import pickle
    		reload(sys)
    		sys.setdefaultencoding("utf-8")



		concrete_list=[u'सभी ',u'उनका ',u'उन ',u'उनकी',u'उनके',u'उनको',u'उनमें',u'उनसे',u'उन्हें',u'उन्होंने',u'उस',u'उसका',u'उसकी',u'उसके',u'उसने',u'जिनकी',u'जिनके',u'जिन',u'जिनमें',u'जिनसे',u'जिन्हें',u'जिन्होंने']
		concrete_list_root=[u'हम',u'वो',u'वहाँ',u'मै',u'जहाँ',u'खुद',u'कौन',u'आपस',u'आप',u'अपना']
		event_list=[u'सब ',u'इसलिये',u'इसलिए']
		extra_list_root=[u'तभी',u'कोई',u'अभी',u'कुछ',u'अब',u'तो',u'किसी']
		time=[u'तब',u'जब']

		dic_total_pronoun_root={}
		dic_correct_pronoun_root={}
		dic_incorrect_pronoun_root={}

		dic_pronoun_dtc={}
		dic_pronoun_svclinear={}
		dic_pronoun_linearsvc={}
		dic_pronoun_bayesian={}
		dic_pronoun_ridge={}

		dic_total_pronoun={}
		dic_correct_pronoun={}
		dic_incorrect_pronoun={}







    		if True:
			#path='propredictionclass/'
			#inputPath = sys.argv[1]
    			#fileList = folderWalk(inputPath)
    			#newFileList = []
			#newFileList.append(inputPath)
        		STR='';c=1;con=0;eve=0
			flagtime=False;flagextra=False
			if True:
			#for fileName in newFileList :
				#print '\n\n\n\nFile Name ::: ',fileName,'\n\n\n'
				c =c +1;matchnode = '';pronounlocaton = 0;verbpredist = 0;verbpostdist = 0;
				d = upperd
				for tree in d.nodeList :
		    			preverb='';postverb='';pronounlocaton = 0;posn0='';
					pos0='';pos1='';pos2='';pos3='';pos4='';strt = ''
		    			for chunkNode in tree.nodeList :
		        			for node in chunkNode.nodeList:
		            				pronounlocaton = pronounlocaton + 1
		            				verbpredist = verbpredist + 1
		            				if node.lex!='NULL' and node.type == 'PRP':
								#print '\n\nNew:\n',node.text
								if node.lex in concrete_list:
									con=con+15
								elif node.morphroot in time:
									con=con+15
									flagtime=True
								elif node.morphroot in concrete_list_root:
									con=con+15
								elif node.lex in event_list:
									eve=eve+16
								elif node.lex in extra_list_root:
									eve=eve+16
									flagextra=True
								else:
									pass
								#print node.text.strip(),con,eve
								if True:
								        co1 = 0;flag = False;flag1 = False;verbpostdist = 0;
								        for t1 in d.nodeList:
								            for c1 in t1.nodeList:
								                for n1 in c1.nodeList:
								                    if flag :
								                        co1 = co1 + 1
								                        if co1 == 1:
								                            pos2 = n1.type
								                            nextword = n1.lex
								                        if co1 == 2:
								                            pos3 = n1.type
								                        if co1 == 3:
								                            pos4 = n1.type
								                            flag = False

								                    if flag1:
								                        verbpostdist = verbpostdist + 1
								                        if n1.type == 'VM':
								                            postverb = n1
								                            flag1 = False
								                    if n1 == node and chunkNode == c1 and t1 == tree:
								                        flag1 = True
								                        flag = True
								                        verbpostdist = 0
									if postverb !='':
										if postverb.getAttribute('stype') == None:
											postverb.addAttribute('stype','')
										if postverb.getAttribute('voicetype') == None:
											postverb.addAttribute('voicetype','')
									if preverb != '':	
										if preverb.getAttribute('stype') == None:
											preverb.addAttribute('stype','')
										if preverb.getAttribute('voicetype') == None:
											preverb.addAttribute('voicetype','')
									if node.getAttribute('semprop') == None:
										node.addAttribute('semprop','')
								        if postverb == '':
										Features='pronoun='+node.lex+' root='+node.morphroot+' number='+node.number+' person='+node.person+' gender='+node.gender+' semprop='+node.getAttribute('semprop')+' chunklength='+str(len(chunkNode.nodeList))+' nextword='+nextword+' POSn0='+posn0+' POS0='+pos0+' POS1='+pos1+' POS2='+pos2+' POS3='+pos3+' POS4='+pos4+' verbpostdist='+str(verbpostdist)+' postverbstype='+'none'+' postverbvt='+'none'+' postverbg='+'none'+' postverbn='+'none'+' postverbp='+'none'+' verbpredist='+str(verbpredist)+' verbpredist='+str(verbpredist)+' preverbstype='+preverb.getAttribute('stype')+' preverbvt='+preverb.getAttribute('voicetype')+' preverbg='+preverb.gender+' preverbn='+preverb.number+' preverbp='+preverb.person+' drel='+node.parentRelation
									elif preverb == '':
										Features='pronoun='+node.lex+' root='+node.morphroot+' number='+node.number+' person='+node.person+' gender='+node.gender+' semprop='+node.getAttribute('semprop')+' chunklength='+str(len(chunkNode.nodeList))+' nextword='+nextword+' POSn0='+posn0+' POS0='+pos0+' POS1='+pos1+' POS2='+pos2+' POS3='+pos3+' POS4='+pos4+' verbpostdist='+str(verbpostdist)+' postverbstype='+postverb.getAttribute('stype')+' postverbvt='+postverb.getAttribute('voicetype')+' postverbg='+postverb.gender+' postverbn='+postverb.number+' postverbp='+postverb.person+' verbpredist='+str(verbpredist)+' verbpredist='+str(verbpredist)+' preverbstype='+'none'+' preverbvt='+'none'+' preverbg='+'none'+' preverbn='+'none'+' preverbp='+'none'+' drel='+node.parentRelation
									else:	
										Features='pronoun='+node.lex+' root='+node.morphroot+' number='+node.number+' person='+node.person+' gender='+node.gender+' semprop='+node.getAttribute('semprop')+' chunklength='+str(len(chunkNode.nodeList))+' nextword='+nextword+' POSn0='+posn0+' POS0='+pos0+' POS1='+pos1+' POS2='+pos2+' POS3='+pos3+' POS4='+pos4+' verbpostdist='+str(verbpostdist)+' postverbstype='+postverb.getAttribute('stype')+' postverbvt='+postverb.getAttribute('voicetype')+' postverbg='+postverb.gender+' postverbn='+postverb.number+' postverbp='+postverb.person+' verbpredist='+str(verbpredist)+' verbpredist='+str(verbpredist)+' preverbstype='+preverb.getAttribute('stype')+' preverbvt='+preverb.getAttribute('voicetype')+' preverbg='+preverb.gender+' preverbn='+preverb.number+' preverbp='+preverb.person+' drel='+node.parentRelation
									measurment1 = []
									measurment2 = []
									tmp1 = {}
									tmp2 = {}
									O=''
									i = Features.strip()
									itemp = i.split()
									for j in range(len(itemp)-1):
										k =itemp[j].split('=')
										tmp1[k[0]]=k[1]
									ptype= itemp[len(itemp)-1].split('=')
									measurment1.append(tmp1)
									vec1 = DictVectorizer(sparse=False)
									with open(path+'features.pkl','rb') as handle:
											vec1.vocabulary_=pickle.loads(handle.read())
									X = vec1.transform(measurment1)
									clf = joblib.load(path+'svm_svr')
									for ic in X:
										if clf.predict(ic)==1:
											con+=1
										else:
											eve+=1

									clf = joblib.load(path+'svm_svc_rbf')
									for ic in X:
										if clf.predict(ic)==1:
											con+=1
										else:
											eve+=1

									clf = joblib.load(path+'svm_svc')
									for ic in X:
										if clf.predict(ic)==1:
											con+=1
										else:
											eve+=1

									clf = joblib.load(path+'t_dtr')
									for ic in X:
										if clf.predict(ic)==1:
											con+=1
										else:
											eve+=1

									clf = joblib.load(path+'t_dtc')
									for ic in X:
										if clf.predict(ic)==1:
											con+=1
										else:
											eve+=1
									clf = joblib.load(path+'svm_svc_linear')
									for ic in X:
										if clf.predict(ic)==1:
											con+=1
										else:
											eve+=1
									clf = joblib.load(path+'svm_linearsvc')
									for ic in X:
										if clf.predict(ic)==1:
											con+=1
										else:
											eve+=1
									clf = joblib.load(path+'linear_model.BayesianRidge')
									for ic in X:
										if clf.predict(ic) > 0.30:
											con+=1
										else:
											eve+=10
									clf = joblib.load(path+'linear_model.Ridge (alpha = .5)')
									for ic in X:
										if clf.predict(ic) > 0.25:
											con+=1
										else:
											eve+=1
									clf = joblib.load(path+'linear_model.Lasso(alpha = 0.1)')
									for ic in X:
										if clf.predict(ic)>0.25:
											con+=1
										else:
											eve+=1
									clf = joblib.load(path+'linear_model.LassoLars(alpha=.1)')
									for ic in X:
										if clf.predict(ic)>0.25:
											con+=1
										else:
											eve+=1
									clf = joblib.load(path+'linear_model.RidgeCV(alphas=[0.1, 1.0, 10.0])')
									for ic in X:
										if clf.predict(ic)>0.25:
											con+=1
										else:
											eve+=1



									#print 'second - time',node.text.strip(),con,eve,flagtime,flagextra
									if con>eve:
										if flagtime:						
											#print con,eve,'ConcreteTime'
											node.addAttribute('reftype','T')
											#print node.text.encode('utf-8')
										else:
											#print con,eve,'Concrete'
											node.addAttribute('reftype','C')
										flagtime=False
										con=0;eve=0
									else:
										if flagextra:						
											#print con,eve,'Eventextra'
											node.addAttribute('reftype','O')
										else:
											#print con,eve,'Event'
											node.addAttribute('reftype','E')
										flagextra=False
										con=0;eve=0
									flagtime=flagextra=False;con=eve=0		
							posn0=pos0
							pos0=pos1
							pos1=node.type
							if node.type == 'VM':
								preverb = node
								verbpredist = 0

						#for tree in d.nodeList:
						#        print tree.printSSFValue(allFeat=False).encode('utf-8')
				return d
