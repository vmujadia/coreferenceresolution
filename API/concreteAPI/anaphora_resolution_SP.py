# -*- coding: utf-8 -*-
import codecs
import re
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle
from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
import argparse


class secondPerson_resolution():
    
    def __init__(self, node):
        self.node=node
        self.chunk=self.node.upper
        self.tree=self.chunk.upper
        self.file=self.tree.upper
        self.verbrootforFP=[u'दोहरा',u'चाह',u'समझ',u'बता',u'बुला',u'कह']
	self.notbyspeakerlist=[u'आपस']
        #self.SPResolve()
        
    def SPResolve(self):
        #print 'in second person resolution'
	speaker=None	
	#prespeaker=None
	flagsentence=False
        flagchunk=False
	sprcheck=False
	sprcount=0
	#print self.tree.name
        for a in reversed(self.file.nodeList):
		for c in reversed(a.nodeList):
			if (int(a.name)<=int(self.tree.name)) and not self.chunk.isPronounresolved :
				if c.getAttribute('speaker')!=None:
					sprcount+=1
					#print c.getAttribute('speaker'),'first' 
					if c.getAttribute('speaker')!=speaker or speaker==None:
						speaker=c.getAttribute('speaker')
						#prespeaker=speaker
					else:
						sprcount-=1
					
					#print speaker,a.name,a.name
					if sprcount==2:
						sprcheck=True
		 				break
					
		if sprcheck and sprcount==2:
			break
	#print speaker,'final'
	posn=None
	notinprplist=False
	for dc in reversed(self.chunk.nodeList):
		posn=dc.text.split('\t')[0].split('.')[0]
		if dc.lex in self.notbyspeakerlist:
			notinprplist=True
	
	sprcheck=False
	flagsentence=False
        flagchunk=False
	sprcheck=False
	#print self.chunk.upper.name,self.chunk.name,speaker
	if speaker!=None and not notinprplist:
		for a in reversed(self.file.nodeList):
			for c in reversed(a.nodeList):
				if (int(a.name)<=int(self.tree.name) or flagsentence) and not self.chunk.isPronounresolved :
					for d in reversed(c.nodeList):
						#print posn , d.getAttribute('posn'),a.name,self.tree.name,d.text.split('\t')[0].split('.')[0]
						if d.text.split('\t')[0].split('.')[0]!=None:
							if int(a.name)==int(self.tree.name) and int(d.text.split('\t')[0].split('.')[0])< int(posn):
								#print speaker,'vandan',a.name
								if d.lex==speaker:
									#print d.lex,c.name,c.upper.name,speaker
									ans=c
								    	if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
								        	self.chunk.PrePronounrefstm=ans.upper
								        	self.chunk.PrePronounrefnode=ans
								        	self.chunk.isPronounresolved=True
										sprcheck=True
									break
							else:
								if d.lex==speaker:
									#print speaker,'nayan',d.lex,a.name
									ans=c
								    	if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
								        	self.chunk.PrePronounrefstm=ans.upper
								        	self.chunk.PrePronounrefnode=ans
								        	self.chunk.isPronounresolved=True
										sprcheck=True
									break
					if sprcheck:
						break
			if sprcheck:
				break
	#if self.chunk.isPronounresolved!=True:
	#	print self.chunk.upper.name,self.chunk.name,speaker












        flagsentence=False
        flagchunk=False
        # looking into same sentence 
        for a in reversed(self.file.nodeList):
            if (a.name==self.tree.name or flagsentence) and not self.chunk.isPronounresolved:
                flagsentence=True
                if int(a.name)-int(self.tree.name)==0:
                    #print 'sentence changed', a.name , self.tree.name
                    for c in reversed(a.nodeList):
                        #print c.name ,c.getAttribute('head')
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if c.morphroot != None:
                                if (c.morphroot in self.verbrootforFP) and (c.type=='VGF' or (c.type=='VGNN' and c.morphroot==u'कह')):
                                    ans=self.find_dep_in_stm(c,'k4', c.upper)
                                    if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                        self.chunk.PrePronounrefstm=ans.upper
                                        self.chunk.PrePronounrefnode=ans
                                        self.chunk.isPronounresolved=True
                                        break
                                        #return self.chunk
                                        #print ans.getAttribute('head'), ans.name , ans.upper.name , 'VANDANMUJADIA',self.chunk.name,ans.upper.name
                   
                    #another rule not so use full
                    #if given pronoun has pl and resolved entity has its r6 child then mark r6 as it`s referent 
                    
        #end looking into same sentence
        #start looking into -1 sentence 
        
                if int(self.tree.name)-int(a.name)==1:
                    #print 'sentence changed', a.name , self.tree.name
                    if not self.chunk.isPronounresolved:
                        for c in reversed(a.nodeList):
                            if (self.chunk.isPronoun and not self.chunk.isPronounresolved):
                                if c.morphroot != None:
                                    if (c.morphroot in self.verbrootforFP) and (c.type=='VGF' or (c.type=='VGNN' and c.morphroot==u'कह')):
                                        ans=self.find_dep_in_stm(c,'rt', c.upper)
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    if not self.chunk.isPronounresolved:
                        for c in reversed(a.nodeList):
                            if (self.chunk.isPronoun and not self.chunk.isPronounresolved):
                                if c.morphroot != None:
                                    if (c.morphroot in self.verbrootforFP):
                                        ans=self.find_dep_in_stm_W_smp(c,'r6', c.upper,c.getAttribute('semprop'))
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
        #end looking into -1 sentence
        
        #start looking into -2 sentence 
        
                if int(self.tree.name)-int(a.name)==2:
                    #print 'sentence changed', a.name , self.tree.name
                    if not self.chunk.isPronounresolved:
                        for c in reversed(a.nodeList):
                            if (self.chunk.isPronoun and not self.chunk.isPronounresolved):
                                if c.morphroot != None:
                                    if (c.morphroot in self.verbrootforFP) and (c.type=='VGF' or (c.type=='VGNN' and c.morphroot==u'कह')):
                                        ans=self.find_dep_in_stm(c,'k1', c.upper)
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
        #end looking into -2 sentence
        return self.chunk                        
                                
                                
    def find_dep(self,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm(self,node,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp(self,node,depL,stmL,semprop):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop:
                ans=a
                break
        return ans
    
    def find_PRP_in_stm(self,PRP_root,chunk):
        ans=None
        flag=False
        for a in reversed(chunk.upper.nodeList):
            if flag:
                for b in a.nodeList:
                    if b.morphroot in PRP_root:
                        ans=a
            if a.name==chunk.name:
                flag=True
        return ans
