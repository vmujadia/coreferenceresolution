# -*- coding: utf-8 -*-
import codecs
import re
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle
from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
import argparse


class thirdPerson_resolution():
    
    def __init__(self, node,chunk):
        self.node=node
        self.chunk=chunk
        self.tree=chunk.upper
        self.file=self.tree.upper
        #self.TPResolve()
    
    def getverbmorph(self,dchunk):
        #print 'VVVVVV',dchunk.name,dchunk.parent
	if dchunk!=None:
		if dchunk.parent!='0' and dchunk.parent!=None and dchunk.name!=dchunk.parent:
			if not re.search(r'VGF.',dchunk.parent,re.M|re.I):
				self.getverbmorph(self.findupperchunk(dchunk.parent))
		return self.findupperchunk(dchunk.parent)

    def findupperchunk(self,vfg):
	for a in (self.chunk.upper.nodeList):
		if a.name==vfg:
			return a
	return None
    def getgender(self,nood):
	for gf in nood.nodeList:
		gfs=gf.gender
	return gfs

    
    def TPResolve(self):
        #print 'in third person resolution',self.chunk.name,self.chunk.upper.name
        flagsentence=False
        flagchunk=False
	verbgender=None
	selfverb=self.getverbmorph(self.chunk)
	for gf in selfverb.nodeList:
		verbgender=gf.gender

        # looking into same sentence # looking into same sentence # looking into same sentence 
        for a in reversed(self.file.nodeList):
            if (a.name==self.tree.name or flagsentence) and not self.chunk.isPronounresolved:
                flagsentence=True
                if int(a.name)-int(self.tree.name)==0:
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'k1u', c.upper,self.chunk.getAttribute('semprop'))
                                    if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                        if ans.isPronoun:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    continue
                                if (c.parentRelation=='pof') and re.search(r'VGF.',c.parent,re.M|re.I)and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and c.morphPOS=='n':
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    continue
                                if (c.parentRelation=='vmod') and re.search(r'VGF',c.parent,re.M|re.I)and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and c.morphPOS=='n':
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    PRP_HValue=None
                    
                    for e in (self.chunk.nodeList):
                        if e.type=='PRP':
                            PRP_HValue=e.lex
                    
                    chunk_no_count=0
                    flagchunk=False
                    for c in reversed(a.nodeList):
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name:
                                    chunk_no_count=0
                                    continue
                                else:
                                    chunk_no_count=chunk_no_count+1
				

                                    for d in c.nodeList:
                                        if d.lex=='':
                                            continue
                                    if chunk_no_count>7:
                                        break
                                    if PRP_HValue =='' and (c.parentRelation=='r6') and self.chunk.number==c.number and self.chunk.getAttribute('semprop')==c.getAttribute('semprop') and c.morphPOS=='n':
                                        ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_emax_num(c,'k1', c.upper,self.chunk.getAttribute('semprop'),'person',self.chunk.number)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'k1', c.upper,self.chunk.getAttribute('semprop'))
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'r6', c.upper,self.chunk.getAttribute('semprop'))
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp(c,'k2', c.upper,self.chunk.getAttribute('semprop'))
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='rt') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
					ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='ccof') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
					ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='r6-k2') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
					ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
		
		    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='rs') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
					ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='r6') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
					ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='ccof') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
					ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                #end looking into same sentence
                #start looking into -1 sentence #start looking into -1 sentence#start looking into -1 sentence
                if int(self.tree.name)-int(a.name)==1:
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='k2') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
					ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='r6-k1') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
					ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break

                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k1', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'r6', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k2', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k4', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'ccof', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    if c.type=='VGF':
                                        ans=self.find_dep_in_stm_W_smp_per(c,'k1', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break

		    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if (c.parentRelation=='r6') and self.chunk.getAttribute('semprop')==c.getAttribute('semprop'):    
                                    if True:
					ans=c
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break  
                                        
                # Start of JoO
                # end of JO                
                #end looking into -1 sentence
                #start looking into -2 sentence
                if int(self.tree.name)-int(a.name)==2:
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k1', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'r6', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k2', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'k4', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                                        
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            for d in reversed(c.nodeList):
                                if d.type=='VM':
                                    ans=self.find_dep_in_stm_W_smp_per(c,'ccof', c.upper,self.chunk.getAttribute('semprop'),self.chunk.person)
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                
                
                for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            if c.type=='VGNN'and c.parentRelation=='r6':
                                    ans=c
                                    if True:
                                        if ans!=None and self.chunk.isPronoun and self.chunk!=ans and (self.getgender(ans)==verbgender or verbgender=='any') :
                                            self.chunk.PrePronounrefstm=ans.upper
                                            self.chunk.PrePronounrefnode=ans
                                            self.chunk.isPronounresolved=True
                                            break
                    
        return self.chunk                        
                                
                                
    def find_dep(self,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm(self,node,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp(self,node,depL,stmL,semprop):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop:
                ans=a
                break
        return ans
    
    def find_dep_in_stm_W_smp_emax_num(self,node,depL,stmL,semprop,emax,number):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop and a.getAttribute('enamex_type')==emax and a.number==number:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp_per(self,node,depL,stmL,semprop,person):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop and a.person==person:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp_per1(self,node,depL,stmL,semprop,person):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop and (a.person==person or (person=='3h' and a.person=='3')or (person=='3' and a.person=='3h')):
                ans=a
                break
        return ans
    def find_PRP_in_stm(self,PRP_root,chunk):
        ans=None
        flag=False
        for a in reversed(chunk.upper.nodeList):
            if flag:
                for b in a.nodeList:
                    if b.morphroot in PRP_root:
                        ans=a
            if a.name==chunk.name:
                flag=True
        return ans

