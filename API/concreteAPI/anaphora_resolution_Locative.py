# -*- coding: utf-8 -*-
import codecs
import re
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle
from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
import argparse

class locative_resolution():
    
    def __init__(self, node):
        self.node=node
        self.chunk=self.node.upper
        self.tree=self.chunk.upper
        self.file=self.tree.upper
        #self.LResolve()
        
    def LResolve(self):
        #print 'in locative resolution'
        flagsentence=False
        flagchunk=False
        # looking into same sentence 
        for a in reversed(self.file.nodeList):
            if (a.name==self.tree.name or flagsentence) and not self.chunk.isPronounresolved:
                flagsentence=True
                if int(a.name)-int(self.tree.name)==0:
                    #print 'sentence changed', a.name , self.tree.name
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.parentRelation=='k7p' or c.parentRelation=='k2p') and c.getAttribute('semprop')==self.chunk.getAttribute('semprop'):
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.morphroot==u'यहां') and c.getAttribute('semprop')=='rest':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
                    flagchunk=False
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.morphroot==u'वहाँ') and c.getAttribute('semprop')=='rest':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
        #end looking into same sentence
        #start looking into -1 sentence 
        
                if int(self.tree.name)-int(a.name)==1:
                    #print 'sentence changed', a.name , self.tree.name
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.parentRelation=='k7p' or c.parentRelation=='k2p') and c.getAttribute('semprop')=='rest':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.morphroot==u'वहाँ') and c.getAttribute('semprop')=='rest':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.morphroot==u'यहां') and c.getAttribute('semprop')=='rest':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
        #end looking into -1 sentence
        #Start between
                for c in reversed(a.nodeList):
                    if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                        flagchunk=True
                        if c.name==self.chunk.name:
                            continue
                        ans=None
                        if (c.parentRelation=='r6-k2') and c.getAttribute('semprop')=='rest':
                            ans=c
                            if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                self.chunk.PrePronounrefstm=ans.upper
                                self.chunk.PrePronounrefnode=ans
                                self.chunk.isPronounresolved=True
                                break
        #End between
        #start looking into -2 sentence 
        
                if int(self.tree.name)-int(a.name)==2:
                    #print 'sentence changed', a.name , self.tree.name
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.parentRelation=='k7p' or c.parentRelation=='k2p') and c.getAttribute('semprop')=='rest':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
        #end looking into -2 sentence
        #start looking into -3 sentence 
        
                if int(self.tree.name)-int(a.name)==3:
                    #print 'sentence changed', a.name , self.tree.name
                    for c in reversed(a.nodeList):
                        if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                            flagchunk=True
                            if c.name==self.chunk.name:
                                continue
                            ans=None
                            if (c.parentRelation=='k7p' or c.parentRelation=='k2p') and c.getAttribute('semprop')=='rest':
                                ans=c
                                if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                    self.chunk.PrePronounrefstm=ans.upper
                                    self.chunk.PrePronounrefnode=ans
                                    self.chunk.isPronounresolved=True
                                    break
        #end looking into -3 sentence
        #exctra work
                for c in reversed(a.nodeList):
                    if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                        flagchunk=True
                        if c.name==self.chunk.name:
                            continue
                        ans=None
                        if c.getAttribute('enamex_type')=='location' and c.getAttribute('semprop')=='rest':
                            ans=c
                            if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                self.chunk.PrePronounrefstm=ans.upper
                                self.chunk.PrePronounrefnode=ans
                                self.chunk.isPronounresolved=True
                                break
                for c in reversed(a.nodeList):
                    if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                        flagchunk=True
                        if c.name==self.chunk.name:
                            continue
                        ans=None
                        if (c.parentRelation=='k7') and c.getAttribute('semprop')=='rest':
                            ans=c
                            if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
                                self.chunk.PrePronounrefstm=ans.upper
                                self.chunk.PrePronounrefnode=ans
                                self.chunk.isPronounresolved=True
                                break
        #end of exctra work
        
        return self.chunk                  
                                
                                
    def find_dep(self,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm(self,node,depL,stmL):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL:
                ans=a
                break
        return ans
    def find_dep_in_stm_W_smp(self,node,depL,stmL,semprop):
        ans=None
        for a in stmL.nodeList:
            if node.name==a.parent and a.parentRelation==depL and a.getAttribute('semprop')==semprop:
                ans=a
                break
        return ans
    
    def find_PRP_in_stm(self,PRP_root,chunk):
        ans=None
        flag=False
        for a in reversed(chunk.upper.nodeList):
            if flag:
                for b in a.nodeList:
                    if b.morphroot in PRP_root:
                        ans=a
            if a.name==chunk.name:
                flag=True
        return ans
