# -*- coding: utf-8 -*-
import codecs
import re
import ssfAPI_minimal as ssf
import random
import numpy as np
import cPickle
from sklearn.feature_extraction import DictVectorizer
from sklearn import tree
import argparse

class ML_base_resolution():
    def __init__(self, node,PRPTYPE,modelpath):
        self.node=node
        self.chunk=self.node.upper
        self.tree=self.chunk.upper
        self.file=self.tree.upper
        self.PRPTYPE=PRPTYPE
	self.modelpath=modelpath
        #print '\n\n\n\n\n\n'
        #self.MLResolve()
        
    def MLResolve(self):
        flagsentence=False
        flagchunk=False
        flagchunk1=True
        chunkdistance=0
        stmdistance=0
        count=0
        testDATASET={}
        countTochunkmapping={}
        # looking into same sentence 
        for a in reversed(self.file.nodeList):
            if (a.name==self.tree.name or flagsentence) and not self.chunk.isPronounresolved:
                flagsentence=True
                if int(self.tree.name)-int(a.name)<4:
                    #print 'sentence changed', a.name , self.tree.name
                    chunk_head=''
                    for c in reversed(a.nodeList):
                        
                        if c.type=='NP':
                            if (c.name==self.chunk.name and self.chunk.isPronoun and  not self.chunk.isPronounresolved) or flagchunk:
                                flagchunk=True
                                if c.name==self.chunk.name and flagchunk1:
                                    chunkdistance=0
                                    flagchunk1=False
                                    continue
                                
                                chunkdistance=chunkdistance+1
                                #print c.name, c.upper.name,c.getAttribute('head')
                                for d in c.nodeList:
                                    if c.type=='DEM':
                                        DEM=1
                                    else:
                                        DEM=0
                                    if c.morphroot==d.morphroot and d.lex!=None:
                                        chunk_head=d.type
                                        if 'PRP' not in chunk_head  or 'NNP' not in chunk_head or 'NNPC' not in chunk_head or 'NN' not in chunk_head or 'NNC' not in chunk_head:
                                            continue
                                        if chunk_head=='':
                                            continue
                                
                                
                                 
                                if chunk_head=='NN' or chunk_head=='NNC':
                                    FNN=1
                                else:
                                    FNN=0
                                
                                if chunk_head=='NNP' or chunk_head=='NNPC':
                                    FNNP=1
                                else:
                                    FNNP=0
                                
                                if chunk_head=='PRP':
                                    FPRP=1
                                else:
                                    FPRP=0
                                
                                if FPRP==0 and FNN==0 and FNNP==0:
                                    FOTR=1
                                else:
                                    FOTR=0
                                
                                stmdistance=int(self.tree.name)-int(a.name)
                                
                                
                                
                                PRPchunksemprop=self.chunk.getAttribute('semprop')
                                if PRPchunksemprop=='' or PRPchunksemprop==None:
                                    PRPchunksemprop='rest'
                                PRPchunknumber=self.chunk.number
                                if PRPchunknumber=='' or PRPchunknumber==None:
                                    PRPchunknumber='rest'
                                PRPchunkperson=self.chunk.person
                                if PRPchunkperson=='' or PRPchunkperson==None:
                                    PRPchunkperson='rest'
                                
                                
                                
                                chunksemprop=c.getAttribute('semprop')
                                if chunksemprop=='' or chunksemprop==None:
                                    chunksemprop='rest'
                                chunknumber=c.number
                                if chunknumber=='' or chunknumber==None:
                                    chunknumber='any'
                                chunkperson=c.person
                                if chunkperson=='' or chunkperson==None:
                                    chunkperson='none'
                                

                                testDATASET[count]={}
                                testDATASET[count]['anaphora']=self.node.lex
                                testDATASET[count]['anaPRP_type']=self.PRPTYPE
                                testDATASET[count]['anasemprop']=PRPchunksemprop
                                testDATASET[count]['anaperson']=PRPchunkperson
                                testDATASET[count]['ananumber']=PRPchunknumber
                                testDATASET[count]['antPOSTag']=chunk_head
                                testDATASET[count]['antNN']=int(FNN)
                                testDATASET[count]['antNNP']=int(FNNP)
                                testDATASET[count]['antPRP']=int(FPRP)
                                testDATASET[count]['antOTR']=int(FOTR)
                                testDATASET[count]['antsemprop']=chunksemprop
                                testDATASET[count]['antperson']=chunkperson
                                testDATASET[count]['antnumber']=chunknumber
                                testDATASET[count]['distance']=int(chunkdistance)
                                testDATASET[count]['stm_distance']=int(stmdistance)
                                testDATASET[count]['DEM']=int(DEM)
                                countTochunkmapping[count]=c
                                count=count+1
        
        dis=[]
        ans=None
        
        with open(self.modelpath, 'rb') as fid:
            gnb_loaded = cPickle.load(fid)
        for i in testDATASET:
            #print testDATASET[i]
            dis=[]
            dis.append(testDATASET[i])
            #print testDATASET[i]

            vec=DictVectorizer(sparse=False)
	    vec.vocabulary_={u'anaphora=\u091c\u093f\u0938\u0928\u0947': 61, 'anaPRP_type=L': 3, 'antNNP': 95, 'anasemprop=rest': 93, u'anaphora=\u0939\u092e\u0928\u0947': 83, 'antperson=3h': 108, u'anaphora=\u0924\u092e': 66, u'anaphora=\u0906\u092a\u0938\u0947': 27, 'ananumber=pl': 9, 'anaPRP_type=JO': 2, 'anasemprop=': 89, 'stm_distance': 117, 'anasemprop=in': 92, 'anaperson=1': 11, u'anaphora=\u091c\u093f\u0938\u0938\u0947': 63, 'anaperson=3': 14, 'anaperson=2': 12, u'anaphora=\u091c\u093f\u0928\u094d\u0939\u0947\u0902': 55, 'antOTR': 96, u'anaphora=\u0909\u0938\u092e\u0947\u0902': 42, u'anaphora=\u091c\u093f\u0938': 57, u'anaphora=\u091c\u094b': 65, 'ananumber=sg': 10, u'anaphora=\u0906\u092a\u0938': 26, 'antPOSTag=NN': 97, u'anaphora=\u0935\u0939\u0940': 78, u'anaphora=\u091c\u093f\u0928': 50, 'antsemprop=h': 113, u'anaphora=\u0915\u093f\u0938\u0915\u0947': 46, u'anaphora=\u0906\u092a\u0915\u0940': 23, u'anaphora=\u0906\u092a\u0915\u0947': 24, u'anaphora=\u0906\u092a\u0915\u094b': 25, 'antnumber=sg': 103, 'DEM': 0, u'anaphora=\u091c\u093f\u0928\u092e\u0947\u0902': 53, u'anaphora=\u0935\u0939\u093e\u0902': 77, u'anaphora=\u0935\u0939\u093e\u0901': 76, 'antsemprop=nh': 114, 'anaPRP_type=SP': 6, u'anaphora=\u0909\u0928\u0938\u0947': 34, u'anaphora=\u0939\u092e': 82, u'anaphora=\u0916\u0941\u0926': 47, u'anaphora=\u0939\u092e\u0947\u0902': 88, u'anaphora=\u092e\u0947\u0930\u093e': 70, u'anaphora=\u091c\u093f\u0928\u094d\u0939\u094b\u0902\u0928\u0947': 56, 'ananumber=any': 8, 'antsemprop=': 111, u'anaphora=\u092e\u0941\u091d\u0938\u0947': 68, u'anaphora=\u0909\u0938\u0940': 44, 'antperson=1': 104, 'antperson=2': 105, 'antperson=3': 107, u'anaphora=\u0906\u092a': 21, u'anaphora=\u092e\u0947\u0930\u0940': 71, u'anaphora=\u092e\u0947\u0930\u0947': 72, u'anaphora=\u091c\u0939\u093e\u0901': 48, u'anaphora=\u091c\u0939\u093e\u0902': 49, u'anaphora=\u0939\u092e\u0938\u0947': 84, 'anaperson=any': 16, u'anaphora=\u0935\u0939\u0940\u0902': 79, 'antperson=2h': 106, 'antPOSTag=PRP': 99, u'anaphora=\u0906\u092a\u0915\u093e': 22, u'anaphora=\u091c\u093f\u0938\u0915\u093e': 58, 'antPRP': 100, u'anaphora=\u0909\u0928\u094d\u0939\u094b\u0902\u0928\u0947': 36, u'anaphora=\u091c\u093f\u0938\u0947': 64, u'anaphora=\u092e\u0948\u0902\u0928\u0947': 74, u'anaphora=\u092e\u0941\u091d\u0947': 69, 'anaPRP_type=FP': 1, u'anaphora=\u091c\u093f\u0938\u092e\u0947\u0902': 62, u'anaphora=\u0909\u0938\u0947': 45, u'anaphora=\u091c\u093f\u0928\u0915\u0947': 52, u'anaphora=\u091c\u093f\u0928\u0915\u0940': 51, 'antNN': 94, u'anaphora=\u0909\u0938': 37, 'antnumber=any': 101, u'anaphora=\u0909\u0928': 28, 'antsemprop=any': 112, u'anaphora=\u0909\u0928\u094d\u0939\u0947\u0902': 35, u'anaphora=\u0939\u092e\u093e\u0930\u0947': 87, 'distance': 116, u'anaphora=\u0939\u092e\u093e\u0930\u0940': 86, u'anaphora=\u0909\u0928\u0915\u093e': 29, u'anaphora=\u0924\u0939\u093e\u0902': 67, u'anaphora=\u091c\u093f\u0938\u0915\u0947': 60, 'antperson=any': 109, u'anaphora=\u091c\u093f\u0938\u0915\u0940': 59, u'anaphora=\u0909\u0938\u0928\u0947': 41, u'anaphora=\u0909\u0928\u0915\u0947': 31, 'anasemprop=h': 91, 'antsemprop=rest': 115, u'anaphora=\u0909\u0928\u0915\u0940': 30, u'anaphora=\u0909\u0938\u0915\u093e': 38, u'anaphora=\u0909\u0928\u0915\u094b': 32, 'antnumber=pl': 102, u'anaphora=\u0935\u0939': 75, u'anaphora=\u091c\u093f\u0928\u0938\u0947': 54, 'anaPRP_type=O': 4, 'anaPRP_type=R': 5, u'anaphora=\u0909\u0938\u0938\u0947': 43, u'anaphora=\u0905\u092a\u0928\u093e': 18, 'anaperson=3h': 15, u'anaphora=\u0909\u0928\u092e\u0947\u0902': 33, u'anaphora=\u0905\u092a\u0928\u0947': 20, u'anaphora=\u0905\u092a\u0928\u0940': 19, u'anaphora=\u0939\u092e\u093e\u0930\u093e': 85, u'anaphora=\u0935\u0947': 80, u'anaphora=\u0935\u094b': 81, u'anaphora=\u092e\u0948\u0902': 73, u'anaphora=\u0909\u0938\u0915\u0947': 40, 'antPOSTag=NNP': 98, u'anaphora=\u0909\u0938\u0915\u0940': 39, 'anaPRP_type=VH': 7, 'anasemprop=any': 90, 'anaperson=2h': 13, 'antperson=none': 110, 'anaperson=none': 17}

            #print vec.vocabulary_
            X=vec.transform(dis)
            #print vec.fit_transform(dis).toarray()
            #print vec.get_feature_names()
            ans=None
            if 'yes' in gnb_loaded.predict(X):
                prd=countTochunkmapping[i]
                if self.chunk.getAttribute('semprop')==prd.getAttribute('semprop'):
                    #print countTochunkmapping[i].name ,countTochunkmapping[i].getAttribute('head')
                    ans=prd
                    break
            else:
                ans=None
                
                
        if ans!=None and self.chunk.isPronoun and self.chunk!=ans:
            self.chunk.PrePronounrefstm=ans.upper
            self.chunk.PrePronounrefnode=ans
            self.chunk.isPronounresolved=True
            
        return self.chunk

    
